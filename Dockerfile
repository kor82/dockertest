FROM php:7.2-cli
COPY index.php /index.php
RUN chmod +x /index.php
ENTRYPOINT ["php", "/index.php"]